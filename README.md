#README.md ---> #(simplified-repertory)#
###---<REPOSITORIO DE APERTURAS SIMPLIFICADAS>---###

La idea de este repositorio es hacer un repertorio de aperturas simples y sólidas siguiendo unas directrices. Se acabaron largas 
líneas teóricas y que las partidas acaben en miniatura por errores idiotas. Las directrices están en "Directrices.pdf" .
Recomendable cierto manejo de INGLÉS.



	PARA APORTAR CON LÍNEAS A LA BASE:
- Crea dos carpetas dentro de "all_sources/". Tendrán el nombre:

wS_<nombre_tuyo>                                (w es white=blancas)
bS_<nombre_tuyo>				(b es black=negras)
	
Dentro de éstas, cread una base de datos de Chessbase (.pgn a ser posible) y meted las líneas que queráis.


  	PARA USAR LA BASE DE DATOS DE APERTURAS:
- Clona el máster en un directorio local de tu computadora, preferiblemente dentro de la carpeta "Bases" de Chessbase.
(en la terminal):

git clone "la url"              

(También puede hacerse desde la web del repositorio en Github: 
https://www.github.com/kelly8080/simplified-repertory )

- Usar Chessbase con las bases "SIMPLE" y disfrutar.


NOTA:
El repo es público así que por favor, no difundir la información a terceras personas ajenas al club.

Un saludo!


###---<END>---###
