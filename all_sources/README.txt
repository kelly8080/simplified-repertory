Base de datos de REPERTORIOS:

-simplified-repertory: Proyecto Github en kelly8080/simplified-repertory. La idea es ir subiendo líneas
a las carpetas para actualizar la base de datos de repertorio final que serán:

white_Simple.cbh
black_Simple.cbh
Simple.cbh 						(white+black)

Para ello ir a la carpeta all_bases, luego crear carpetas:

wS_<nombre>
bS_<nombre>

Dentro de éstas carpetas, hacer los aportes de las líneas conforme las directrices en el pdf: 

SIMPLE-directrices.pdf

Luego, se irán actualizando la base SIMPLE con todos los aportes.

Gracias!